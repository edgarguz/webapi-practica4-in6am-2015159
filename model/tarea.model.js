var database = require('../config/database.config');
var Tarea = {};

Tarea.selectAll = function(callback) {
	if(database) {
		var consulta = 'SELECT * FROM Tarea';
		database.query(consulta, function(error, resultado){
			if(error) throw error;
			callback(resultado);
		});
	}
}

Tarea.select = function(idUsuario, callback) {
  if(database) {
		database.query('CALL sp_VerTareas(?)', idUsuario,
     function(error, resultados){
			if(error) {
				throw error;
			} else {
				callback(resultados[0]);
			}
		});
	}
}

Tarea.insert = function(data, callback) {
  if(database) {
    database.query("CALL sp_agregarTarea(?,?,?,?)",
    [data.titulo, data.descripcion, data.fechaFinal, data.idUsuario],
    function(error, resultado) {
      if(error) throw error;
      callback({"affectedRows": resultado.affectedRows});
    });
  }
}


module.exports = Tarea;
