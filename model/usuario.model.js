var database = require('../config/database.config');
var Usuario = {};
// var usuarioModel = {};
Usuario.selectAll = function(callback) {
  if(database) {
    database.query('CALL sp_verUsuarios();',
    function(error, resultados) {
      if(error) throw error;
      if(resultados.length > 0) {
        callback(resultados);
      } else {
        callback(0);
      }
    })
  }
}

Usuario.find = function(idUsuario, callback) {
  if(database) {
    var consulta = 'SELECT * FROM Usuario WHERE idUsuario = ?'; //agregado
    database.query(consulta, idUsuario, function(error, resultados) {
      if(error) throw error;
      if(resultados.length > 0) {
        callback(resultados[0]); // editado
      } else {
        callback(0);
      }
    })
  }
}

Usuario.insert = function(data, callback) {
  if(database) {
    database.query("call sp_insertUsuario(?,?)", [data.nick, data.contrasena],
    function(error, resultado) {
      if(error) throw error;
      callback({"affectedRows": resultado.affectedRows});
    });
  }
}

Usuario.update = function(data, callback) {
  if(database) {
    var sql = "call sp_updateUsuario(?,?,?,?)";
    database.query(sql,
    [data.nick, data.contrasena, data.picture, data.idUsuario],
    function(error, resultado) {
      if(error) throw error;
      callback(resultado);
    });
  }
}

Usuario.delete = function(idUsuario, callback) {
  if(database) {
    var sql = "CALL sp_eliminarUsuario(?)";
    database.query(sql, idUsuario,
    function(error, resultado) {
      if(error) throw error;
      callback({"Mensaje": "Eliminado"});
    });
  }
}

Usuario.autenticar = function(data, callback) {
  if(database) {
    var consulta = 'CALL sp_autenticarUsuario(?, ?);';
		database.query(consulta, [data.nick, data.contrasena], function(error, resultado){
			if(error) {
				throw error;
			} else {
				callback(resultado[0]);
			}
		});
	}
}

module.exports = Usuario;
