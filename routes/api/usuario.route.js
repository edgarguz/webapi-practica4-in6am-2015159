var express = require('express');
var usuarioModel = require('../../model/usuario.model');
var usuarioRoute = express.Router();
// services?
usuarioRoute.get('/usuario', function(req, res) {
    usuarioModel.selectAll(function(resultados) {
      if(resultados !== 0) {
        res.json(resultados);
      }else{
        res.json({"mensaje":"No hay resultado"});
      }
    });
  });
  usuarioRoute.post('/usuario',function(req, res) {
    var data = {
      nick: req.body.nick,
      contrasena: req.body.contrasena
    }
    usuarioModel.insert(data, function(resultado){
    if(resultado.affectedRows > 0) {
        res.json(resultado);
      } else {
        res.json({"Mensaje":"No se pudo registrar"});
      }
    });
  });

usuarioRoute.get('/usuario/:idUsuario', function(req, res) {
    var idUsuario = req.params.idUsuario;
    usuarioModel.find(idUsuario, function(resultados){
      if(resultados !== 0) { // editar?
        res.json(resultados);
      } else {
        res.json({"Mensaje": "No se encontro el usuario"})
      }
    });
  });

  usuarioRoute.put('/usuario/:idUsuario', function(req, res) {
    var idUsuario = req.params.idUsuario;
    var data = {
      idUsuario: req.body.idUsuario,
      nick: req.body.nick,
      contrasena: req.body.contrasena,
      picture: req.body.picture
    }

    if(idUsuario == data.idUsuario) {
      usuarioModel.update(data, function(resultado){
        if(typeof resultado !== undefined){//editado
          res.json(resultado);
        } else {
          res.json({"mensaje":"Error al actualizar"})
        }
      });
    }else{
      res.json({"mensaje":"No concuerdan los ids"})
    }
  })
  usuarioRoute.delete('/usuario/:idUsuario', function(req, res) {
    var idUsuario = req.params.idUsuario;
    usuarioModel.delete(idUsuario, function(resultado){
      if(resultado && resultado.Mensaje === "Eliminado") {
        res.json({"Mensaje":"se ha eliminado el usuario"});
      } else {
        res.json({"Mensaje": "No se elimino`"});
      }
    });
  });


module.exports = usuarioRoute;
