var express = require('express');
var tarea = require('../../model/tarea.model');
var router = express.Router();
var uri = '/tarea';

// middleware that is specific to this router
// router.use(function timeLog(req, res, next) {
//   console.log('Time: ', Date.now());
//   next();
// });

router.get(uri, function(req, res) {
  tarea.selectAll(function(resultado){
    if(typeof resultado !== undefined) {
      res.json(resultado);
    } else {
      res.json({"mensaje" : "No hay tareas"});
    }
  });
});

router.get(uri+'/:idUsuario', function(req, res) {
  var idUsuario = req.params.idUsuario;
  tarea.select(idUsuario, function(resultados){
    if(resultados !== 0){
      res.json(resultados);
    }else {
      res.json({"mensaje":"No hay tareas"});
    }
  });
});

router.post('/tarea', function(req,res) {

  var data ={
    titulo: req.body.titulo,
    descripcion: req.body.descripcion,
    fechaFinal: req.body.fechaFinal,
    idUsuario: req.body.idUsuario
  }
  tarea.insert(data, function(resultado) {
    if(resultado.affectedRows > 0) {
        res.json(resultado);
      } else {
        res.json({"Mensaje":"No se pudo agregar"});
      }
  });
});

module.exports = router;
